---
- name: install required packages
  yum:
    name: "{{ ioc_base_packages }}"
    state: present

- name: update fstab
  mount:
    path: "{{ item.mountpoint }}"
    src: "{{ item.src }}"
    fstype: nfs4
    opts: "{{ item.opts | default('ro') }}"
    state: "{{ ioc_base_is_anaconda_installation | ternary('present', 'mounted') }}"
  loop: "{{ ioc_base_nfs_mountpoints }}"
  tags:
    - update-fstab

- name: create the group for the ioc user
  group:
    name: "{{ ioc_base_group_name }}"
    gid: "{{ ioc_base_group_id }}"

- name: create the ioc user for running iocs
  user:
    name: "{{ ioc_base_user_name }}"
    uid: "{{ ioc_base_user_id }}"
    group: "{{ ioc_base_group_name }}"
    system: true

# See https://epics-controls.org/resources-and-support/documents/howto-documents/channel-access-reach-multiple-soft-iocs-linux/
# We use ifup-local because NetworkManager is not enabled
- name: create script to convert UDP CA incoming requests to broadcast
  template:
    src: ifup-local.j2
    dest: /sbin/ifup-local
    owner: root
    group: root
    mode: 0755
  tags:
    - ioc-iptables
  notify:
    - run ifup-local

- name: create host directory on nonvolatile nfs server
  file:
    path: "{{ ioc_base_nonvolatile_source }}"
    state: directory
    owner: "{{ ioc_base_user_id }}"
    group: root
    mode: 0755
  delegate_to: "{{ ioc_base_nonvolatile_server }}"
  when: ioc_base_nonvolatile_server != ""  # noqa 602
  tags:
    - nonvolatile

- name: mount nonvolatile directory
  mount:
    path: "{{ ioc_base_nonvolatile_directory }}"
    src: "{{ ioc_base_nonvolatile_server }}:{{ ioc_base_nonvolatile_source }}"
    fstype: nfs4
    opts: rw
    state: "{{ ioc_base_is_anaconda_installation | ternary('present', 'mounted') }}"
  when: ioc_base_nonvolatile_server != ""  # noqa 602
  tags:
    - nonvolatile

# Some IOCs use /opt/shared, so create a symlink
# We could of course use the ioc_base_nonvolatile_directory variable
# but it's easier to have both locations on all IOCs (rahter than having
# to find wich path is used on which IOC).
# In the end all IOCs should use the same path
- name: create /opt/shared link to /opt/nonvolatile
  file:
    src: "{{ ioc_base_nonvolatile_directory }}"
    dest: /opt/shared
    state: link
    follow: false
  when: ioc_base_nonvolatile_server != ""  # noqa 602
  tags:
    - nonvolatile

- name: create /var/log/procServ directory
  file:
    path: /var/log/procServ
    owner: "{{ ioc_base_user_name }}"
    group: "{{ ioc_base_group_name }}"
    mode: 0755
    state: directory

- name: configure logrotate for procServ
  copy:
    src: procserv
    dest: /etc/logrotate.d/
    owner: root
    group: root
    mode: 0644

# The task is allowed to fail because this is for testing
# and the repo might not exist for all machines
- name: clone IOC repository
  git:
    repo: "{{ ioc_base_gitlab_url }}/{{ ioc_base_gitlab_namespace }}/{{ ansible_fqdn }}.git"
    dest: "{{ ioc_base_iocs_folder }}"
    version: "{{ ioc_base_ioc_version }}"
    force: true
    update: true
  ignore_errors: true
  no_log: true
  tags:
    - ioc-update

# If one file changes, we restart all the services
# to avoid having to create separate handlers and tasks
# All the services are linked anyway
- name: create services to autostart IOCs
  template:
    src: "{{ item }}.j2"
    dest: "/etc/systemd/system/{{ item }}"
    owner: root
    group: root
    mode: 0644
  loop:
    - ioc-legacy@.service
    - ioc-master.service
    - ioc@.service
  notify: restart ioc-master

# Don't check that all services are started
# They don't all always run but they are started by the handler
- name: ensure ioc-master is enabled
  systemd:
    name: ioc-master.service
    daemon_reload: "{{ not ioc_base_is_anaconda_installation }}"
    enabled: true

- name: install ess-boot service
  include_tasks: install-ess-boot.yml
  when: ioc_base_boot_service

- name: remove ess-boot service
  include_tasks: remove-ess-boot.yml
  when: not ioc_base_boot_service
