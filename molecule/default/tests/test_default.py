import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_procserv_installed(host):
    cmd = host.run('/usr/bin/procServ --version')
    assert cmd.stdout.startswith('procServ Process Server')


def test_autostart_ioc_service_enabled(host):
    assert host.service("ioc-master.service").is_enabled
    if host.ansible.get_variables()["inventory_hostname"] == "ics-ans-role-ioc-base-default.example.org":
        assert host.service("ess-boot.service").is_enabled
    else:
        assert not host.file("/etc/systemd/system/ess-boot.service").exists


def test_iptables_for_ca_incoming_requests(host):
    rules = host.iptables.rules("nat", "PREROUTING")
    assert len(rules) == 2
    assert "-P PREROUTING ACCEPT" == rules[0]
    assert "-A PREROUTING" in rules[1]
    assert "--dport 5064" in rules[1]


def test_ioc_cloned_repo(host):
    if host.ansible.get_variables()["inventory_hostname"] == "ics-ans-role-ioc-base-default.example.org":
        assert host.file("/opt/iocs/MY-IOC/production/st.cmd").exists
    else:
        assert not host.file("/opt/iocs").exists
